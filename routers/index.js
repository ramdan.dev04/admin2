const express = require("express");
const { isLoggedin, isLoggedout, Logoutme } = require("../auth/Authcheck");
const {
  Login,
  Register,
  postLogin,
  postUser,
} = require("../controllers/controller");
const { Dashboard } = require("../controllers/controller");
const route = express.Router();

route.get("/login", isLoggedout, Login);
route.get("/logout", Logoutme);
route.get("/register", isLoggedout, Register);
route.post("/login", isLoggedout, postLogin);
route.post("/register", isLoggedout, postUser);

route.get("/*", isLoggedin, Dashboard);

module.exports = route;
