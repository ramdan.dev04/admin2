const http = require("http");

exports.Dashboard = (req, res) => {
  var opts = {
    host: "localhost",
    port: 3000,
    path: req.url,
    method: req.method,
    headers: req.headers,
    agent: false,
  };

  var request = http.request(opts);

  request.on("response", function (response) {
    // set status code
    if (response.statusCode) res.status(response.statusCode);

    // set headers
    if (response.headers) res.set(response.headers);

    response.pipe(res);
  });

  req.pipe(request);
};
