const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const userSchema = new Schema({
  email: String,
  password: String,
  class: String,
});
const licenseSchema = new Schema({
  email: String,
  key: String,
  registered: Number,
  expired: Number,
});

exports.Users = mongoose.model("users", userSchema);
exports.License = mongoose.model("licenses", licenseSchema);
