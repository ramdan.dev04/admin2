const { hashPw, validatePw } = require("../libs/helper");
const { Users } = require("./models");
const jwt = require("jsonwebtoken");

exports.validateEmail = async (email) => {
  let user = await Users.findOne({ email });
  return user;
};

exports.authLogin = async (email, password, req, res) => {
  let user = await Users.findOne({ email });
  if (user) {
    await validatePass(password, user.password, req, res, email);
  } else {
    res.json({ code: 404 });
  }
};

exports.registerUser = async (email, password, res) => {
  let pass = await hashPw(password);
  let data = {
    email,
    password: pass,
    class: "user",
  };
  let check = await regValidate(email);
  if (!check) {
    return res.json({ code: 302 });
  }
  let write = new Users(data);
  let stats = await write.save();
  if (stats) {
    return res.json({ code: 200 });
  } else {
    return res.json({ code: 500 });
  }
};

const regValidate = async (email) => {
  let check = await Users.findOne({ email });
  if (check) {
    return false;
  }
  return true;
};

const validatePass = async (pass, hash, req, res, email) => {
  let check = await validatePw(pass, hash);
  if (check) {
    var token = await jwt.sign({ email }, `${process.env.TOKEN_SCRT}`, {
      expiresIn: "3600s",
    });
    req.session.user = { token };
    return res.json({ code: 200, token });
  }
  return res.json({ code: 402 });
};
