const { gql } = require("apollo-server-express");

const typeDefs = gql`
  type Query {
    users: [user!]!
    user: member!
    license: license
  }

  type user {
    id: ID!
    email: String!
    password: String!
    class: String!
  }

  type member {
    id: ID!
    email: String!
    class: String!
  }

  type license {
    id: ID!
    email: String!
    key: String!
    registered: String!
    expired: String!
  }
`;

module.exports = typeDefs;
