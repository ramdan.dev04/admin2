const moment = require("moment");
const { Users, License } = require("./models");

const resolvers = {
  Query: {
    users: (_, {}, context) => {
      if (!context || context.user.class != "admin") {
        return new Error("unauthorized");
      }
      return Users.find();
    },
    user: (_, {}, context) => {
      if (!context || context.user == null) return new Error("unauthorize");
      return Users.findOne({ email: context.user.email });
    },
    license: (_, {}, context) => {
      if (!context || context.user == null) return new Error("unauthorize");
      return License.findOne({ email: context.user.email });
    },
  },
};

module.exports = resolvers;
