const express = require("express");
const { json, urlencoded } = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const route = require("./routers");
const app = express();
require("dotenv").config({ path: ".env" });
const mongoose = require("mongoose");
const { ApolloServer } = require("apollo-server-express");
const typeDefs = require("./models/typeDefs");
const resolvers = require("./models/resolvers");
const path = require("path");
const apiAuth = require("./auth/apiAuth");

mongoose.connect(process.env.MONGO_URI, { useUnifiedTopology: true }, (err) => {
  if (err) console.log(err);
});

app.use(urlencoded({ extended: true }));
app.use(json());
app.use(cookieParser());
app.use(
  session({ secret: "Admur123", saveUninitialized: false, resave: false })
);

const apidb = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    var tokens = req.headers.authorization || "";
    tokens = tokens.replace(/Bearer /i, "");
    if (!tokens || tokens == null) {
      return { user: false };
    }
    const user = await apiAuth(tokens);
    return { user };
  },
});
app.use("/assets", express.static("views/assets"));
app.use("/static", express.static("views/static"));
app.use("/manifest.json", express.static("views/manifest.json"));
app.use("/logo192.png", express.static("views/logo192.png"));
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "./views"));
app.use("/", route);

apidb.applyMiddleware({ app });

app.listen(process.env.PORT).on("listening", () => console.log("listening"));
