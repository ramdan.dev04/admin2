const { authLogin, registerUser } = require("../models/auth");

exports.Login = function (req, res) {
  res.render("login", { title: "Users Login" });
};

exports.Register = (req, res) => {
  res.render("register", { title: "User Register" });
};

exports.postLogin = (req, res) => {
  let body = req.body;
  authLogin(body.email, body.password, req, res);
};

exports.postUser = (req, res) => {
  let body = req.body;
  registerUser(body.email, body.password, res);
};

exports.Dashboard = (req, res) => {
  res.render("index");
};
