const { tokenVerify } = require("../libs/token");
const { validateEmail } = require("../models/auth");

exports.isLoggedin = async (req, res, next) => {
  if (typeof req.session.user != "undefined" && req.session.user != null) {
    let checkTok = await tokenVerify(req.session.user.token);
    if (!checkTok) {
      req.session.user = null;
      res.cookie("token", "", { maxAge: 1 });
      return res.redirect("/login");
    }
    let check = await validateEmail(checkTok.email);
    if (!check) {
      req.session.user = null;
      res.cookie("token", "", { maxAge: 1 });
      return res.redirect("/login");
    }
    res.cookie("token", req.session.user.token);
    next();
  } else {
    let cookies = req.cookies;
    if (typeof cookies.token != "undefined" && cookies.token != null) {
      let test = await tokenVerify(cookies.token);
      if (!test) {
        res.cookie("token", "", { maxAge: 1 });
        return res.redirect("/login");
      } else {
        let check = await validateEmail(`${test.email}`);
        if (!check) {
          res.cookie("token", "", { maxAge: 1 });
          return res.redirect("/login");
        }
        req.session.user = { token: cookies.token };
        return next();
      }
    }
    res.cookie("token", "", { maxAge: 1 });
    res.redirect("/login");
  }
};

exports.isLoggedout = async (req, res, next) => {
  if (typeof req.session.user != "undefined" && req.session.user != null) {
    res.redirect("/");
  } else {
    if (typeof req.cookies.token != "undefined") {
      let token = req.cookies.token;
      let veriftok = await tokenVerify(token);
      if (veriftok) {
        let checkem = await validateEmail(veriftok.email);
        if (checkem) {
          return res.redirect("/");
        }
      }
    }
    res.cookie("token", "", { maxAge: 1 });
    next();
  }
};

exports.Logoutme = (req, res) => {
  req.session.user = null;
  res.cookie("token", "", { maxAge: 1 });
  res.redirect("/login");
};
