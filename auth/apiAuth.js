const { tokenVerify } = require("../libs/token");
const { Users } = require("../models/models");

const apiAuth = async (token) => {
  let check = await tokenVerify(token);
  if (!check) {
    return false;
  }
  let user = await Users.findOne({ email: check.email });
  if (!user) {
    return false;
  }
  return user;
};

module.exports = apiAuth;
