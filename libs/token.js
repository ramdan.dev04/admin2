const jwt = require("jsonwebtoken");

exports.tokenVerify = async (token) => {
  try {
    let start = await jwt.verify(token, process.env.TOKEN_SCRT);
    return start;
  } catch (error) {
    if (error) return false;
  }
};
