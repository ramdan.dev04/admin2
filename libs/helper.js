const bcrypt = require("bcrypt");

exports.hashPw = async (pass) => {
  let hashed = await bcrypt.hash(pass, 10);
  return hashed;
};

exports.validatePw = async (pass, hash) => {
  let valid = await bcrypt.compare(pass, hash);
  return valid;
};
