const Login = () => {
  let form = document.getElementById("loginform");
  form.addEventListener("submit", (e) => {
    e.preventDefault();
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    fetch("/login", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((response) => response.json())
      .then((res) => {
        if (res.code == 200) {
          document.cookie = `token=${res.token}`;
          Swal.fire({
            icon: "success",
            title: "Logging you in",
            showConfirmButton: false,
            timer: 1500,
            timerProgressBar: true,
          });
          setTimeout(() => {
            window.location.replace("../../");
          }, 1700);
        } else if (res.code == 404) {
          Swal.fire({
            icon: "error",
            title: "Account doesn't exist",
            text: "We can't find your account",
          });
        } else if (res.code == 402) {
          Swal.fire({
            icon: "error",
            title: "Wrong password",
            text: "We can't validate the given password",
          });
        }
      });
  });
};
