const Register = () => {
  let form = document.getElementById("regform");
  form.addEventListener("submit", (e) => {
    e.preventDefault();
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    fetch("/register", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((response) => response.json())
      .then((res) => {
        if (res.code == 200) {
          Swal.fire({
            icon: "success",
            title: "Redirecting you",
            showConfirmButton: false,
            timer: 1500,
            timerProgressBar: true,
          });
          setTimeout(() => {
            window.location.replace("../../login");
          }, 1700);
        } else if (res.code == 302) {
          Swal.fire({
            icon: "error",
            title: "Account exist",
            text: "This email already registered",
          });
        } else if (res.code == 500) {
          Swal.fire({
            icon: "error",
            title: "Something went wrong",
            text: "We can't registering you, Please contact admin",
          });
        }
      });
  });
};
